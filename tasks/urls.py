from django.urls import path
from tasks.views import create_task, user_task_filter

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", user_task_filter, name="show_my_tasks"),
]
